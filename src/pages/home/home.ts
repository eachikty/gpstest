import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {GpsPage} from '../gps/gps';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  goGps() {
    this.navCtrl.push(GpsPage);
   

  }
}
