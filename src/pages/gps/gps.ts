import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LaunchNavigator} from '@ionic-native/launch-navigator';

/**
 * Generated class for the GpsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gps',
  templateUrl: 'gps.html',
})
export class GpsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private launchNavigator: LaunchNavigator) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GpsPage');
  }

  goGps(){
    this.launchNavigator.navigate('KFC Terminal 21');
  }

}
